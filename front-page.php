<?php get_header(); ?>
<body <?php body_class(); ?>
<?php get_template_part( 'template-parts/page/content', 'front-page' ); ?>


<div id="primary" class="content-area container">
	<main id="main" class="site-main" role="main">
		<div class="flex_container">
		<?php
		$newslist = get_posts(array(
		  'posts_per_page' => 9
// 		  'category_name' => 'news'
		));
		?>
		<?php if(!empty($newslist)) :?>
		<?php
  foreach ( $newslist as $post ) :
    setup_postdata( $post );
  ?>
				<?php get_template_part( 'template-parts/post/content', 'grid' ); ?>
			<?php endforeach; ?>
 <?php wp_reset_postdata(); ?>
			

		</div><!-- .flex_container -->
		<?php else:?>
		<?php get_template_part( 'template-parts/post/content', 'none' ); ?>
		<?php endif;?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>

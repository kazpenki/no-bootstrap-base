<div class="fb-share-button" 
    data-href="<?php the_permalink(); ?>" 
    data-layout="button_count">
  </div>
<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>">Tweet</a>
<div class="g-plus" data-action="share" data-annotation="none"></div>

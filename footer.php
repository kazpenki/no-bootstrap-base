<div class="footer_wrap" id="footer">
<div class="container">
	<footer>
		<span class="copyright">Copyright &copy; <?php echo date('Y'); ?> <a href="<?php echo home_url(); ?>"><?php echo get_bloginfo('name') ?></a> <br class="visible-xs">All right reserved.</span>
	</footer>

</div>
</div>

<?php wp_footer(); ?>
<?php get_template_part('include/snsbtn_script'); ?>
<?php get_template_part('include/to-top'); ?>
<?php get_template_part('include/navfix'); ?>
</body>
</html>
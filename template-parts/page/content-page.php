<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php
			the_content();

		?>
		<?php if (is_page('sitemap')) { ?>
		<h3>固定ページ</h3>
		<ul>
		<?php wp_list_pages('title_li='); ?>
		</ul>
		<h3>記事一覧</h3>
		<ul>
		<?php get_archives('postbypost','','html'); ?>
		</ul>
		<?php }; ?>
		
		<h3>月別アーカイブ</h3>
		<ul>
		<?php wp_get_archives( 'type=monthly' ); ?>
		</ul>
	</div><!-- .entry-content -->
</article><!-- #post-## -->

<div class="front_flex_box_wrap">

	<div class="front_flex_box">
	<a class="front_flex_caption" href="<?php the_permalink(); ?>">
		<?php the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); ?>
		<?php the_excerpt(); ?>
	</a><!-- .front_flex_caption -->

		


				<?php if (has_post_thumbnail()): ?>
				<?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
				<?php else: ?>
				<img src="<?php echo get_template_directory_uri();echo'/assets/images/no-image-fs8.png'; ?>">


	<?php endif; ?>
	</div>



</div><!-- .front_flex_box -->


<div class="headblock">
	<div class="container-fluid">
		<h1 class="site_title"><a href="<?php echo home_url(); ?>"><?php echo get_bloginfo('name') ?></a></h1>
		<p class="site_description"><?php echo get_bloginfo('description') ?></p>
	</div>
</div>